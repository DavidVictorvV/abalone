var mouseDown = 0;
document.body.onmousedown = function () {
  ++mouseDown;
};
document.body.onmouseup = function () {
  --mouseDown;
};
var canvasWidth = window.innerWidth;
var canvasHeight = window.innerHeight;
var table;
var positions = [];
var blackPieces = [];
var r = 30;
var player = 1;

function setup() {
  createCanvas(800, 600);
  gameTable();
  blackTeam();
}
function draw() {
  background(51, 10, 10);
  text("X: " + mouseX, 10, 30);
  text("Y: " + mouseY, 10, 50);

  fill(150);
  polygon(400, 300, 300, 6);
  polygon(400, 300, 250, 6);
  //   circle(400, 300, 30);
  fill(180);
  positions.forEach((p) => {
    p.drawPiece();
  });
  blackPieces.forEach((p) => {
    p.drawPiece();
  });
  // selectPiece();
  movePiece();
}

function blackTeam() {
  nrOfPieces = 15;
  length = 5;
  for (i = 0; i < positions.length; i++) {
    if (nrOfPieces > 0) {
      fill("black");
      blackPieces.push(new gamePiece(positions[i].x, positions[i].y, r));
      positions[i].setTeam("BLACK");
      blackPieces[i].setTeam("BLACK");
      nrOfPieces--;
    } else {
      blackPieces.push(new gamePiece(positions[i].x, positions[i].y, r));
      blackPieces[i].setColor("red");
    }
  }
  blackPieces[11].setColor("gray");
  blackPieces[11].setTeam("EMPTY");
  positions[11].setTeam("EMPTY");
  blackPieces[12].setColor("gray");
  positions[12].setTeam("EMPTY");
  blackPieces[12].setTeam("EMPTY");
  positions[15].setTeam("BLACK");
  blackPieces[15].setColor("black");
  blackPieces[15].setTeam("BLACK");
}

function movePiece() {
  for (i = 0; i < blackPieces.length; i++) {
    if (
      mouseX >= blackPieces[i].x - r / 2 &&
      mouseX <= blackPieces[i].x + r / 2 &&
      mouseY >= blackPieces[i].y - r / 2 &&
      mouseY <= blackPieces[i].y + r / 2 &&
      blackPieces[i].color == "black" &&
      mouseDown
    ) {
      pos = i;
      // blackPieces[i].setColor("white");
      seeEmptySpots(i);
    }
  }

  for (i = 0; i < blackPieces.length; i++) {
    if (
      mouseX >= blackPieces[i].x - r / 2 &&
      mouseX <= blackPieces[i].x + r / 2 &&
      mouseY >= blackPieces[i].y - r / 2 &&
      mouseY <= blackPieces[i].y + r / 2 &&
      blackPieces[i].team == "purple" &&
      blackPieces[i].color == "purple" &&
      mouseDown
    ) {
      blackPieces[i].setColor("orange");
    }
  }

  for (i = 0; i < positions.length; i++) {
    if (
      mouseX >= positions[i].x - r / 2 &&
      mouseX <= positions[i].x + r / 2 &&
      mouseY >= positions[i].y - r / 2 &&
      mouseY <= positions[i].y + r / 2 &&
      positions[i].team == "EMPTY" &&
      positions[i].color == "green" &&
      mouseDown
    ) {
      blackPieces[i].setColor("black");
      blackPieces[pos].setColor("gray");
      blackPieces[pos].setColor("gray");
      positions[pos].setTeam("EMPTY");
    }
  }
}

function seeEmptySpots(pos) {
  this.pos = pos;
  for (i = 0; i < positions.length; i++) {
    if (positions[i].team == "EMPTY") {
      positions[i].setColor("gray");
    }

    if (
      ((positions[i].y - 47 == blackPieces[pos].y &&
        (positions[i].x + 25 == blackPieces[pos].x ||
          positions[i].x - 25 == blackPieces[pos].x)) ||
        (positions[i].y + 47 == blackPieces[pos].y &&
          (positions[i].x + 25 == blackPieces[pos].x ||
            positions[i].x - 25 == blackPieces[pos].x)) ||
        (positions[i].y == blackPieces[pos].y &&
          positions[i].x - 50 == blackPieces[pos].x) ||
        (positions[i].y == blackPieces[pos].y &&
          positions[i].x + 50 == blackPieces[pos].x)) &&
      positions[i].team == "EMPTY"
    ) {
      positions[i].setColor("green");
    }
  }
  // seeBlackSpotsClose(pos);
}

function seeBlackSpotsClose(pos) {
  this.pos = pos;
  for (i = 0; i < blackPieces.length; i++) {
    blackPieces[pos].setColor("orange");
    if (
      ((positions[i].y - 47 == blackPieces[pos].y &&
        (positions[i].x + 25 == blackPieces[pos].x ||
          positions[i].x - 25 == blackPieces[pos].x)) ||
        (positions[i].y + 47 == blackPieces[pos].y &&
          (positions[i].x + 25 == blackPieces[pos].x ||
            positions[i].x - 25 == blackPieces[pos].x)) ||
        (positions[i].y == blackPieces[pos].y &&
          positions[i].x - 50 == blackPieces[pos].x) ||
        (positions[i].y == blackPieces[pos].y &&
          positions[i].x + 50 == blackPieces[pos].x)) &&
      positions[i].team == "BLACK"
    ) {
      blackPieces[i].setColor("purple");
      blackPieces[i].setTeam("purple");
    }
  }
}

class gamePiecePos {
  constructor(x, y, r) {
    this.x = x;
    this.y = y;
    this.r = r;
    this.team = "EMPTY";
    this.color = "gray";
    circle(this.x, this.y, this.r);
  }
  setTeam(team) {
    this.team = team;
  }
  drawPiece() {
    fill(this.color);
    circle(this.x, this.y, this.r);
  }

  setColor(color) {
    this.color = color;
  }
}

class gamePiece {
  constructor(x, y, r) {
    this.x = x;
    this.y = y;
    this.r = r;
    this.pos = false;
    this.color = "black";
    this.team = "EMPTY";
    circle(this.x, this.y, this.r);
  }
  drawPiece() {
    if (
      this.color == "black" ||
      this.color == "green" ||
      this.color == "orange" ||
      this.color == "purple"
    ) {
      fill(this.color);
      circle(this.x, this.y, this.r);
    }
  }
  setColor(color) {
    this.color = color;
  }

  setTeam(team) {
    this.team = team;
  }
}

function polygon(x, y, radius, npoints) {
  let angle = TWO_PI / npoints;
  beginShape();
  for (let a = 0; a < TWO_PI; a += angle) {
    let sx = x + cos(a) * radius;
    let sy = y + sin(a) * radius;
    vertex(sx, sy);
  }
  endShape(CLOSE);
}

function gameTable() {
  xDefault = 300;
  x = xDefault;
  y = 110;
  length = 5;
  for (i = 0; i < 5; i++) {
    for (j = 0; j < length; j++) {
      positions.push(new gamePiecePos(x, y, r));
      x += 50;
    }
    length++;
    x = xDefault - 25;
    xDefault -= 25;
    y += 47;
  }
  xDefault += 50;
  length -= 2;
  x = xDefault;
  for (i = 0; i < 4; i++) {
    for (j = 0; j < length; j++) {
      positions.push(new gamePiecePos(x, y, 30));
      x += 50;
    }
    y += 47;
    x = xDefault + 25;
    xDefault += 25;
    length--;
  }
}
