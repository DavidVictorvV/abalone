var mouseDown = 0;
document.body.onmousedown = function () {
  ++mouseDown;
};
document.body.onmouseup = function () {
  --mouseDown;
};
var gameTableItems = [];
var outPiece = [];
var r = 30;
var reset = false;
var team = "WHITE";
var selected = 0;
var clicked = 0;
var pos = 61;
var posM = [];
var dirSelect = "ANY";

function setup() {
  createCanvas(800, 600);
  gameTable();
  teams();
  // gameTableItems[21].setColor("white");
  // gameTableItems[21].setTeam("WHITE");
  outOfGame();
}
function draw() {
  background("#534524");
  fill("red");
  for (i = 0; i < gameTableItems.length; i++) {
    if (
      mouseX >= gameTableItems[i].x - r / 2 &&
      mouseX <= gameTableItems[i].x + r / 2 &&
      mouseY >= gameTableItems[i].y - r / 2 &&
      mouseY <= gameTableItems[i].y + r / 2
    ) {
      text("X: " + gameTableItems[i].x, 10, 30);
      text("Y: " + gameTableItems[i].y, 10, 50);
    }
  }

  fill(150);
  polygon(400, 300, 300, 6);
  polygon(400, 300, 250, 6);

  gameTableItems.forEach((p) => {
    p.drawPiece();
  });
  outPiece.forEach((p) => {
    p.drawPiece();
  });
  selectPiece();

  movePiece();
}

function piecePos() {
  for (i = 0; i < gameTableItems.length; i++) {
    if (
      mouseX >= gameTableItems[i].x - r / 2 &&
      mouseX <= gameTableItems[i].x + r / 2 &&
      mouseY >= gameTableItems[i].y - r / 2 &&
      mouseY <= gameTableItems[i].y + r / 2 &&
      mouseDown &&
      gameTableItems[i].team == team
    ) {
      pos = 1;
    }
    return pos;
  }
}

function mouseClicked() {
  for (i = 0; i < gameTableItems.length; i++) {
    if (
      mouseX >= gameTableItems[i].x - r / 2 &&
      mouseX <= gameTableItems[i].x + r / 2 &&
      mouseY >= gameTableItems[i].y - r / 2 &&
      mouseY <= gameTableItems[i].y + r / 2
    ) {
      pos = i;
      posM.push(i);
    } else if (mouseX < 200) {
      pos = 61;
    }
  }
}
function selectPiece() {
  for (i = 0; i < gameTableItems.length; i++) {
    if (pos == 61) {
      if (gameTableItems[i].team == "SELECTED") {
        gameTableItems[i].setTeam("WHITE");
        gameTableItems[i].setColor("white");
        selected = 0;
      }
    }
    if (pos == 61 || selected == 3) {
      if (gameTableItems[i].team == "PURPLE") {
        gameTableItems[i].setTeam("WHITE");
        gameTableItems[i].setColor("white");
      }
    }
  }

  for (i = 0; i < gameTableItems.length; i++) {
    if (i == pos) {
      if (
        gameTableItems[pos].team != "SELECTED" &&
        gameTableItems[pos].team != "PURPLE"
      ) {
        for (j = 0; j < gameTableItems.length; j++) {
          if (
            gameTableItems[j].team == "SELECTED" ||
            gameTableItems[j].team == "PURPLE"
          ) {
            gameTableItems[j].setTeam("WHITE");
            gameTableItems[j].setColor("white");
            selected = 0;
            dirSelect = "ANY";
          }
        }
      }

      if (selected == 0 && gameTableItems[i].team == team) {
        gameTableItems[pos].setColor("orange");
        gameTableItems[pos].setTeam("SELECTED");
        seeNextSelectablePiece(pos);

        selected++;
      } else if (gameTableItems[i].team == "PURPLE" && selected < 3) {
        gameTableItems[pos].setColor("orange");
        gameTableItems[pos].setTeam("SELECTED");
        selected++;
        selectDir();
        seeNextSelectablePiece(pos);
      }
    }
  }
}

function selectDir() {
  for (i = 0; i < gameTableItems.length - 1; i++) {
    if (gameTableItems[i].team == "PURPLE") {
      gameTableItems[i].setTeam("WHITE");
      gameTableItems[i].setColor("white");
    }
  }

  for (i = 0; i < posM.length; i++) {
    if (dirSelect == "ANY") {
      if (
        gameTableItems[posM[posM.length - 1]].x ==
          gameTableItems[posM[posM.length - 2]].x - 25 &&
        gameTableItems[posM[posM.length - 1]].y ==
          gameTableItems[posM[posM.length - 2]].y - 47
      ) {
        dirSelect = "UL";
      } else if (
        gameTableItems[posM[posM.length - 1]].x ==
          gameTableItems[posM[posM.length - 2]].x + 25 &&
        gameTableItems[posM[posM.length - 1]].y ==
          gameTableItems[posM[posM.length - 2]].y - 47
      ) {
        dirSelect = "UR";
      } else if (
        gameTableItems[posM[posM.length - 1]].x ==
          gameTableItems[posM[posM.length - 2]].x + 25 &&
        gameTableItems[posM[posM.length - 1]].y ==
          gameTableItems[posM[posM.length - 2]].y + 47
      ) {
        dirSelect = "DR";
      } else if (
        gameTableItems[posM[posM.length - 1]].x ==
          gameTableItems[posM[posM.length - 2]].x - 25 &&
        gameTableItems[posM[posM.length - 1]].y ==
          gameTableItems[posM[posM.length - 2]].y + 47
      ) {
        dirSelect = "DL";
      } else if (
        gameTableItems[posM[posM.length - 1]].y ==
          gameTableItems[posM[posM.length - 2]].y &&
        gameTableItems[posM[posM.length - 1]].x - 50 ==
          gameTableItems[posM[posM.length - 2]].x
      ) {
        dirSelect = "R";
      } else dirSelect = "L";
    }
  }
}

function seeNextSelectablePiece(pos) {
  this.pos = pos;
  for (j = 0; j < gameTableItems.length; j++) {
    if (gameTableItems[j].team == team) {
      if (gameTableItems[j].y - 47 == gameTableItems[pos].y) {
        if (
          gameTableItems[j].x - 25 == gameTableItems[pos].x &&
          (dirSelect == "ANY" || dirSelect == "DR" || dirSelect == "UL")
        ) {
          gameTableItems[j].setColor("purple");
          gameTableItems[j].setTeam("PURPLE");
        }
        if (
          gameTableItems[j].x + 25 == gameTableItems[pos].x &&
          (dirSelect == "ANY" || dirSelect == "DL")
        ) {
          gameTableItems[j].setColor("purple");
          gameTableItems[j].setTeam("PURPLE");
        }
      }

      if (gameTableItems[j].y == gameTableItems[pos].y - 47) {
        if (
          gameTableItems[j].x == gameTableItems[pos].x + 25 &&
          (dirSelect == "ANY" || dirSelect == "UR")
        ) {
          gameTableItems[j].setColor("purple");
          gameTableItems[j].setTeam("PURPLE");
        }
        if (
          gameTableItems[j].x == gameTableItems[pos].x - 25 &&
          (dirSelect == "ANY" || dirSelect == "UL" || dirSelect == "DR")
        ) {
          gameTableItems[j].setColor("purple");
          gameTableItems[j].setTeam("PURPLE");
        }
      }
      if (gameTableItems[j].y == gameTableItems[pos].y) {
        if (
          gameTableItems[j].x - 50 == gameTableItems[pos].x &&
          (dirSelect == "ANY" || dirSelect == "R")
        ) {
          gameTableItems[j].setColor("purple");
          gameTableItems[j].setTeam("PURPLE");
        }
        if (
          gameTableItems[j].x + 50 == gameTableItems[pos].x &&
          (dirSelect == "ANY" || dirSelect == "L")
        ) {
          gameTableItems[j].setColor("purple");
          gameTableItems[j].setTeam("PURPLE");
        }
      }
    }
  }
}

function movePiece() {}

function outOfGame() {
  x = 243;
  y = 505;
  dir = 1;
  for (i = 0; i < 2; i++) {
    for (j = 0; j < 5; j++) {
      outPiece.push(new gamePiece(x, y, 30));
      x -= 26 * dir;
      y -= 45;
    }
    x += 26;
    y -= 5;
    dir *= -1;
  }
  x = 557;
  y = 505;
  dir = 1;
  for (i = 0; i < 2; i++) {
    for (j = 0; j < 5; j++) {
      outPiece.push(new gamePiece(x, y, 30));
      x += 26 * dir;
      y -= 45;
    }
    x -= 26;
    y -= 5;
    dir *= -1;
  }
  x = 300;
  y = 540;
  for (i = 0; i < 2; i++) {
    for (j = 0; j < 5; j++) {
      outPiece.push(new gamePiece(x, y, 30));
      x += 50;
    }
    x = 300;
    y = 60;
  }
  for (i = 0; i < outPiece.length; i++) {
    outPiece[i].setColor("#9d937e");
  }
}

class gamePiece {
  constructor(x, y, r) {
    this.x = x;
    this.y = y;
    this.r = r;
    this.pos = false;
    this.color = "gray";
    this.team = "EMPTY";
    circle(this.x, this.y, this.r);
  }

  drawPiece() {
    if (this.team != "SELECTED") {
      fill(this.color);
      circle(this.x, this.y, this.r);
    } else {
      fill("orange");
      circle(this.x, this.y, this.r);
    }
  }

  setColor(color) {
    this.color = color;
  }

  setTeam(team) {
    this.team = team;
  }
}

function teams() {
  nrOfPieces = 14;
  for (i = 0; i < gameTableItems.length; i++) {
    if (nrOfPieces > 0 && i != 12 && i != 11) {
      gameTableItems[i].setTeam("BLACK");
      gameTableItems[i].setColor("black");
      nrOfPieces--;
    }
  }
  nrOfPieces = 14;
  for (i = gameTableItems.length - 1; i >= 0; i--) {
    if (nrOfPieces > 0 && i != 48 && i != 49) {
      gameTableItems[i].setTeam("WHITE");
      gameTableItems[i].setColor("white");
      nrOfPieces--;
    }
  }
}

function gameTable() {
  xDefault = 300;
  x = xDefault;
  y = 110;
  length = 5;
  for (i = 0; i < 5; i++) {
    for (j = 0; j < length; j++) {
      gameTableItems.push(new gamePiece(x, y, r));
      x += 50;
    }
    length++;
    x = xDefault - 25;
    xDefault -= 25;
    y += 47;
  }
  xDefault += 50;
  length -= 2;
  x = xDefault;
  for (i = 0; i < 4; i++) {
    for (j = 0; j < length; j++) {
      gameTableItems.push(new gamePiece(x, y, r));
      x += 50;
    }
    y += 47;
    x = xDefault + 25;
    xDefault += 25;
    length--;
  }
}

function polygon(x, y, radius, npoints) {
  let angle = TWO_PI / npoints;
  beginShape();
  for (let a = 0; a < TWO_PI; a += angle) {
    let sx = x + cos(a) * radius;
    let sy = y + sin(a) * radius;
    vertex(sx, sy);
  }
  endShape(CLOSE);
}
